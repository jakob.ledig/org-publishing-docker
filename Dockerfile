FROM ubuntu:latest

RUN apt-get update -qq && apt-get upgrade -y && \
    apt-get install -y \tzdata && \
    apt-get install -y -qq pandoc \
            texlive-latex-base \
            texlive-latex-extra \
            texlive-latex-recommended \
            texlive-fonts-recommended \
            emacs \
            org-mode \
            brotli
